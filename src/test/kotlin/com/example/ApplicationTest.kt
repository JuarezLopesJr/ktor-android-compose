package com.example

import com.example.models.ApiResponse
import com.example.repository.HeroRepository
import com.example.repository.NEXT_PAGE_KEY
import com.example.repository.PREV_PAGE_KEY
import io.ktor.application.Application
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.koin.java.KoinJavaComponent.inject

class ApplicationTest {
    private val heroRepository by inject<HeroRepository>(HeroRepository::class.java)

    private fun calculatePage(page: Int): Map<String, Int?> {
        var prevPage: Int? = page
        var nextPage: Int? = page

        if (page in 1..4) {
            nextPage = nextPage?.plus(1)
        }

        if (page in 2..5) {
            prevPage = prevPage?.minus(1)
        }

        if (page == 1) {
            prevPage = null
        }

        if (page == 5) {
            nextPage = null
        }

        return mapOf(PREV_PAGE_KEY to prevPage, NEXT_PAGE_KEY to nextPage)
    }

    /*
     Example reference
    @Test
     fun `access root endpoint, assert correct info`() {
          Application::module refers to main application module
         withTestApplication(moduleFunction = Application::module) {
             handleRequest(HttpMethod.Get, "/").apply {
                 assertEquals(
                     expected = HttpStatusCode.OK,
                     actual = response.status()
                 )
                 assertEquals(
                     expected = "Anime Server API",
                     actual = response.content
                 )
             }
         }
     }

     @Test
     fun `access all heroes endpoint, assert correct info`() {
         withTestApplication(moduleFunction = Application::module) {
             handleRequest(HttpMethod.Get, "/boruto/heroes").apply {
                 assertEquals(
                     expected = HttpStatusCode.OK,
                     actual = response.status()
                 )
                 val expected = ApiResponse(
                     success = true,
                     message = "Ok",
                     prevPage = null,
                     nextPage = 2,
                     heroes = heroRepository.page1
                 )
                 val actual = Json.decodeFromString<ApiResponse>(response.content.toString())
                 println("EXPECTED: $expected")
                 println("ACTUAL: $actual")
                 assertEquals(
                     expected = expected,
                     actual = actual
                 )
             }
         }
     }*/

    @Test
    fun `access all heroes endpoint, query all pages, assert correct info`() {
        withTestApplication(moduleFunction = Application::module) {
            val pages = 1..5

            val heroes = listOf(
                heroRepository.page1,
                heroRepository.page2,
                heroRepository.page3,
                heroRepository.page4,
                heroRepository.page5
            )

            pages.forEach { page ->
                handleRequest(HttpMethod.Get, "/boruto/heroes?page=$page").apply {
                    println("CURRENT PAGE: $page")
                    assertEquals(
                        expected = HttpStatusCode.OK,
                        actual = response.status()
                    )
                    val actual = Json.decodeFromString<ApiResponse>(response.content.toString())

                    val expected = ApiResponse(
                        success = true,
                        message = "Ok",
                        prevPage = calculatePage(page)[PREV_PAGE_KEY],
                        nextPage = calculatePage(page)[NEXT_PAGE_KEY],
                        heroes = heroes[page - 1],
                        lastUpdated = actual.lastUpdated
                    )

                    println("PREV_PAGE: ${calculatePage(page)[PREV_PAGE_KEY]}")
                    println("NEXT_PAGE: ${calculatePage(page)[NEXT_PAGE_KEY]}")
                    println("HEROES: ${heroes[page - 1]}")

                    assertEquals(
                        expected = expected,
                        actual = actual
                    )
                }
            }
        }
    }

    @Test
    fun `access all heroes endpoint, query non existing page, assert error`() {
        withTestApplication(moduleFunction = Application::module) {
            handleRequest(HttpMethod.Get, "/boruto/heroes?page=6").apply {
                assertEquals(
                    expected = HttpStatusCode.NotFound,
                    actual = response.status()
                )

                val expected = ApiResponse(
                    success = false,
                    message = "Heroes not found"
                )

                val actual = Json.decodeFromString<ApiResponse>(response.content.toString())

                assertEquals(
                    expected = expected,
                    actual = actual
                )
            }
        }
    }

    @Test
    fun `access all heroes endpoint, query invalid page parameter, assert error`() {
        withTestApplication(moduleFunction = Application::module) {
            handleRequest(HttpMethod.Get, "/boruto/heroes?page=six").apply {
                assertEquals(
                    expected = HttpStatusCode.BadRequest,
                    actual = response.status()
                )

                val expected = ApiResponse(
                    success = false,
                    message = "Only numbers allowed"
                )

                val actual = Json.decodeFromString<ApiResponse>(response.content.toString())

                assertEquals(
                    expected = expected,
                    actual = actual
                )
            }
        }
    }

    @Test
    fun `search heroes endpoint, query name, assert single hero result`() {
        withTestApplication(moduleFunction = Application::module) {
            handleRequest(HttpMethod.Get, "/boruto/heroes/search?name=sas").apply {
                assertEquals(
                    expected = HttpStatusCode.OK,
                    actual = response.status()
                )

                val actual = Json.decodeFromString<ApiResponse>(response.content.toString())
                    .heroes.size

                assertEquals(
                    expected = 1,
                    actual = actual
                )
            }
        }
    }

    @Test
    fun `search heroes endpoint, query name, assert multiple hero result`() {
        withTestApplication(moduleFunction = Application::module) {
            handleRequest(HttpMethod.Get, "/boruto/heroes/search?name=sa").apply {
                assertEquals(
                    expected = HttpStatusCode.OK,
                    actual = response.status()
                )

                val actual = Json.decodeFromString<ApiResponse>(response.content.toString())
                    .heroes.size

                assertEquals(
                    expected = 3,
                    actual = actual
                )
            }
        }
    }

    @Test
    fun `search heroes endpoint, query empty string, assert empty list result`() {
        withTestApplication(moduleFunction = Application::module) {
            handleRequest(HttpMethod.Get, "/boruto/heroes/search?name= ").apply {
                assertEquals(
                    expected = HttpStatusCode.OK,
                    actual = response.status()
                )

                val actual = Json.decodeFromString<ApiResponse>(response.content.toString())
                    .heroes

                assertEquals(
                    expected = emptyList(),
                    actual = actual
                )
            }
        }
    }

    @Test
    fun `search heroes endpoint, query non-existing hero, assert empty list result`() {
        withTestApplication(moduleFunction = Application::module) {
            handleRequest(HttpMethod.Get, "/boruto/heroes/search?name=unknown").apply {
                assertEquals(
                    expected = HttpStatusCode.OK,
                    actual = response.status()
                )

                val actual = Json.decodeFromString<ApiResponse>(response.content.toString())
                    .heroes

                assertEquals(
                    expected = emptyList(),
                    actual = actual
                )
            }
        }
    }

    @Test
    fun `access non-existing endpoint, assert not found`() {
        withTestApplication(moduleFunction = Application::module) {
            handleRequest(HttpMethod.Get, "/unknown").apply {
                assertEquals(
                    expected = HttpStatusCode.NotFound,
                    actual = response.status()
                )

                assertEquals(
                    expected = "Page not found",
                    actual = response.content
                )
            }
        }
    }
}