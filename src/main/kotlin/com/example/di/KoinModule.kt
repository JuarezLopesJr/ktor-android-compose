package com.example.di

import com.example.repository.HeroRepository
import com.example.repository.HeroRepositoryImpl
import org.koin.dsl.module

val koinModule = module {
    /* setting as a singleton */
    single<HeroRepository> {
        HeroRepositoryImpl()
    }
}