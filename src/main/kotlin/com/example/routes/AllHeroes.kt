package com.example.routes

import com.example.models.ApiResponse
import com.example.repository.HeroRepository
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import org.koin.ktor.ext.inject

const val PAGE_PARAMETER = "page"

fun Route.getAllHeroes() {
    val heroRepository by inject<HeroRepository>()

    get("/boruto/heroes") {
        try {
            val page = call.request.queryParameters[PAGE_PARAMETER]?.toInt() ?: 1
            /* this method throws the IllegalArgumentException in the second catch
               it forces the API to serve only pages in this range (0 also throws an error) */
            require(page in 1..5)

            val apiResponse = heroRepository.getAllHeroes(page)

            /* this method should always be the last instruction */
            call.respond(
                message = apiResponse,
                status = HttpStatusCode.OK
            )

        } catch (e: NumberFormatException) {
            call.respond(
                message = ApiResponse(success = false, message = "Only numbers allowed"),
                status = HttpStatusCode.BadRequest
            )
        } catch (e: IllegalArgumentException) {
            call.respond(
                message = ApiResponse(success = false, message = "Heroes not found"),
                status = HttpStatusCode.NotFound
            )
        }
    }
}