package com.example.routes

import com.example.repository.HeroRepository
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import org.koin.ktor.ext.inject

const val QUERY_PARAMETER = "name"

fun Route.searchHeroes() {
    val heroRepository by inject<HeroRepository>()

    get("/boruto/heroes/search") {
        val name = call.request.queryParameters[QUERY_PARAMETER]
        val apiResponse = heroRepository.searchHeroes(name)

        call.respond(
            message = apiResponse,
            status = HttpStatusCode.OK
        )
    }
}