package com.example.plugins

import com.example.di.koinModule
import io.ktor.application.Application
import io.ktor.application.install
import org.koin.ktor.ext.Koin

fun Application.configureKoin() {
    install(Koin) {
//        slf4jLogger()
        modules(koinModule)
    }
}